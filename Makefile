install:
	composer install

start: stop
	symfony serve --daemon

stop:
	symfony server:stop

deploy:
	composer install --no-dev --optimize-autoloader
