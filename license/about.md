I create lots of things, mostly software, and I put them on the Internet for free.
As an author I have the right to decide under which conditions do I waive my copyrights.
Common practice is to just pick one of the permissive licenses,
many of which aim to maximise the user's freedom…

But I don't care.

The whole point of me giving away stuff for free is to make the world a slightly better place –
so if someone wants to use them for evil, then screw their freedom. 
I'm queer, I'm a member of minoritised communities – and I can't just blindly worship “freedom”
in a world where so many use their freedom to actively hurt the most vulnerable.

I don't want my work to be _freely_ used – I want it used _for good_.

So, my license prohibits any use by big corporations, cops, military, or use in a bigoted or violent way.

You're free to use it too, just keep in mind that I am not a lawyer.
This license is inspired by and based on
[Leftcopy](https://leftcopy.org/),
[The Social Domain](https://thesocialdomain.org/),
[The Hippocratic License](https://firstdonoharm.dev/),
[ACAB License](https://github.com/jgrey4296/acab/blob/main/LICENSE),
[the fuck around and find out license](https://paste.sr.ht/~boringcactus/ed023ccf9d7a5559612d6e60f0474d6c3375349d),
[The Anti-Capitalist Software License](https://anticapitalist.software/),
[The JSON License](https://json.org/license.html),
[CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en),
[Jamie Kyle's MIT License](https://github.com/jamiebuilds/license),
[Copilot resistant licenses](https://github.com/big-tech-sux/resistant-licenses),
and the [anti-license manifesto](https://www.boringcactus.com/2021/09/29/anti-license-manifesto.html).
