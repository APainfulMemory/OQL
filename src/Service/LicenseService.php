<?php

namespace App\Service;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class LicenseService
{
    private string $licenseDir;
    private \Parsedown $parsedown;
    private array $versions;
    private array $usages;

    public function __construct(string $licenseDir, \Parsedown $parsedown)
    {
        $this->licenseDir = $licenseDir;
        $this->parsedown = $parsedown;
        $this->versions = array_map(
            fn ($path) => mb_substr($path, mb_strrpos($path, DIRECTORY_SEPARATOR) + 2, -3),
            glob($this->licenseDir . '/v*.md')
        );
        usort($this->versions, fn ($a, $b) => floatval($a) <=> floatval($b));
        $this->usages = array_filter(explode("\n", file_get_contents($licenseDir . '/usages.txt')));
    }

    public function load(?string $version, $licensor = null)
    {
        $version = $this->normaliseVersion($version);

        $path = sprintf('%s/v%s.md', $this->licenseDir, $version);
        $tldrPath = sprintf('%s/v%s.tldr.html', $this->licenseDir, $version);

        if (!file_exists($path) || !file_exists($tldrPath)) {
            throw new NotFoundHttpException();
        }

        $raw = file_get_contents($path);

        $licensor = $this->normaliseLicensor($licensor);

        if ($licensor) {
            $raw = str_replace('{Licensor}', $this->licensorMarkdown($licensor), $raw);
        }

        $parsed = $this->parsedown->parse($raw);

        $parsed = str_replace(
            '<a href="http',
            '<a target="_blank" rel="noopener" href="http',
            $parsed,
        );

        return [
            'version' => $version,
            'raw' => $raw,
            'parsed' => $parsed,
            'title' => trim(mb_substr(explode("\n", $raw)[0], 6)),
            'tldr' => file_get_contents($tldrPath),
            'licensor' => $licensor,
        ];
    }

    public function listVersions(): array
    {
        return $this->versions;
    }

    public function latest(): string
    {
        return $this->versions[count($this->versions) - 1];
    }

    public function about(): string
    {
        return $this->parsedown->parse(file_get_contents(sprintf('%s/about.md', $this->licenseDir)));
    }

    private function normaliseVersion(?string $version)
    {
        if (!$version) {
            return $this->latest();
        }

        if (mb_strpos($version, '.') === false) {
            $version .= '.0';
        }

        return $version;
    }

    private function normaliseLicensor($licensor): ?array
    {
        if ($licensor === null) {
            return null;
        }

        if (!is_array($licensor)) {
            $licensor = [$licensor];
        }

        return array_map(
            function ($l) {
                $parts = explode('|', $l);
                return [
                    'name' => $this->sanitise($parts[0]),
                    'link' => $parts[1] ?? null
                            ? $this->sanitise($parts[1])
                            : null,
                ];
            },
            $licensor
        );
    }

    private function licensorMarkdown(array $licensor): string
    {
        return join(', ', array_map(
            function ($l) {
                return $l['link']
                    ? sprintf('[%s](%s)', $l['name'], $l['link'])
                    : $l['name'];
            },
            $licensor
        ));
    }

    private function sanitise(?string $s)
    {
        $s = str_replace("\r\n", '', $s);
        $s = str_replace("\n", '', $s);
        if (filter_var($s, FILTER_VALIDATE_URL) === false) {
            $s = preg_replace('/([\\\\`*_{}\[\]()#+\!])/', '\\\\$1', $s);
        }

        return trim(strip_tags($s));
    }

    public function listUsages(): array
    {
        return $this->usages;
    }
}
